/*
 Navicat Premium Data Transfer

 Source Server         : local-root
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : xeg_rbac

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 19/11/2020 16:18:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for xeg_admin
-- ----------------------------
DROP TABLE IF EXISTS `xeg_admin`;
CREATE TABLE `xeg_admin`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_login_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `admin_status` tinyint(0) NOT NULL DEFAULT 1 COMMENT '用户状态(0 异常，1正常)',
  `deleted` tinyint(0) NULL DEFAULT 0 COMMENT '逻辑删除（0 删除，1未删）',
  `delete_time` timestamp(0) NULL DEFAULT NULL COMMENT '删除时间',
  `admin_sort` int(0) NULL DEFAULT 0 COMMENT '排序',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '手机号',
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理平台账户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_admin
-- ----------------------------
INSERT INTO `xeg_admin` VALUES (1, '1001', 'admin', '$2a$10$UaLvlGwkr2Q1ngMbL44Kp.nWC0001yVMalj7xXJdXbJq/fqVVOuQW', '2020-10-21 09:07:03', '2020-11-10 19:34:07', 1, 0, NULL, 1, '超级管理员', '0');
INSERT INTO `xeg_admin` VALUES (5, '11111', 'L5NGZHPBY', '$2a$10$2XUj6G0/cGmuJ5GPzkaNaeHcJ.Mp8vy0lfrv7FJ6MJOgootQTRSKq', '2020-11-10 19:36:44', NULL, 1, 0, NULL, 0, '...', '0');
INSERT INTO `xeg_admin` VALUES (6, '22222', '73CJA99JU', '$2a$10$elEI2yVXEdxkzfuDiEiwreNEjUXGpI6njOVXnHRit/9T.E2sGNR2G', '2020-11-10 19:40:13', NULL, 1, 0, NULL, 0, '...', '0');
INSERT INTO `xeg_admin` VALUES (7, 'ningpeng', '95HWE9D7T', '$2a$10$vjPYCFtT/KU.lgDwmmlsn.h4oybBHA5rCZCLnAOPesLbDzqXsOSBa', '2020-11-10 19:46:57', NULL, 1, 0, NULL, 0, '...', '0');
INSERT INTO `xeg_admin` VALUES (8, 'zhengyu', '17676YWXK', '$2a$10$tPVGiIjoToezYic2rbFms.Jcw9dEQUWbrnqNQOTjm1f7rdvdsanSe', '2020-11-10 19:51:09', NULL, 1, 0, NULL, 0, '...', '0');
INSERT INTO `xeg_admin` VALUES (9, 'sans', 'sans', '$2a$10$UHNibpOT8TpRCBQvid4kDuo/.ycfTWCAUCE5.ENmb5O5x4aCh5hZa', '2020-11-19 15:45:30', NULL, 1, 0, NULL, 0, NULL, '0');

-- ----------------------------
-- Table structure for xeg_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `xeg_admin_role`;
CREATE TABLE `xeg_admin_role`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT,
  `admin_id` int(0) NOT NULL DEFAULT 0 COMMENT '账号编号',
  `role_id` int(0) NOT NULL DEFAULT 0 COMMENT '角色编号',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '0 未删除 1已删除',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '账号角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_admin_role
-- ----------------------------
INSERT INTO `xeg_admin_role` VALUES (1, 9, 2, 0, '2020-11-19 15:45:30');

-- ----------------------------
-- Table structure for xeg_agent
-- ----------------------------
DROP TABLE IF EXISTS `xeg_agent`;
CREATE TABLE `xeg_agent`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT,
  `admin_id` int(0) NOT NULL COMMENT '账号编号',
  `agent_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '代理名称',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '创建人',
  `parent_id` int(0) NOT NULL DEFAULT 0 COMMENT '上级代理编号',
  `agent_type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '代理类型  1 一级代理 2 二级代理',
  `area_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '区域编号',
  `invite_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '6位邀请码',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '代理商信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_agent
-- ----------------------------
INSERT INTO `xeg_agent` VALUES (1, 0, '超级管理员', 'admin', 1, 1, '', 'SIKTG9', '2020-10-23 14:58:40', 0);
INSERT INTO `xeg_agent` VALUES (5, 0, '平台一级代理', 'admin', 1, 1, '', '9P9DX2', '2020-11-10 19:36:44', 0);
INSERT INTO `xeg_agent` VALUES (6, 0, '平台二级代理', 'admin', 5, 2, '', 'L1I3YU', '2020-11-10 19:40:13', 0);
INSERT INTO `xeg_agent` VALUES (7, 0, '宁鹏', 'admin', 1, 1, '', 'LK3EQ4', '2020-11-10 19:46:57', 0);
INSERT INTO `xeg_agent` VALUES (8, 0, '正宇', 'admin', 7, 2, '', 'HW5Q8W', '2020-11-10 19:51:09', 0);
INSERT INTO `xeg_agent` VALUES (9, 0, '主持人18678967543', 'admin', 0, 2, '', '000000', '2020-11-10 20:00:48', 0);
INSERT INTO `xeg_agent` VALUES (10, 0, '主持人13406730807', 'admin', 0, 2, '', '000000', '2020-11-10 20:57:16', 0);

-- ----------------------------
-- Table structure for xeg_agent_identity
-- ----------------------------
DROP TABLE IF EXISTS `xeg_agent_identity`;
CREATE TABLE `xeg_agent_identity`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT,
  `agent_id` int(0) NOT NULL DEFAULT 0 COMMENT '代理编号',
  `identity_id` int(0) NOT NULL DEFAULT 0 COMMENT '身份编号',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '手机号',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0 未删除  1已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `role_id` int(0) NOT NULL DEFAULT 0 COMMENT '角色编号',
  `charge` tinyint(0) NOT NULL DEFAULT 0 COMMENT '服务费0-99/100',
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '代理身份关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_agent_identity
-- ----------------------------

-- ----------------------------
-- Table structure for xeg_identity
-- ----------------------------
DROP TABLE IF EXISTS `xeg_identity`;
CREATE TABLE `xeg_identity`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT,
  `identity_type` tinyint(0) NOT NULL COMMENT '身份类型 1 劳务市场 2劳务派遣企业 3 劳务派遣工人 4 主持人 5 企业联系人',
  `identity_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '身份名称 1 劳务市场 2劳务派遣企业 3 劳务派遣工人 4 主持人 5 企业联系人',
  `status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '1普通 2代理',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '0未删除 1已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户身份信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_identity
-- ----------------------------

-- ----------------------------
-- Table structure for xeg_role
-- ----------------------------
DROP TABLE IF EXISTS `xeg_role`;
CREATE TABLE `xeg_role`  (
  `bid` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `is_del` tinyint(0) NULL DEFAULT 0 COMMENT '是否删除(0 删除，1 未删)',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '删除时间',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `role_sort` int(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_role
-- ----------------------------
INSERT INTO `xeg_role` VALUES (1, '超级管理员', 0, NULL, '2020-10-22 18:53:06', 1);
INSERT INTO `xeg_role` VALUES (2, '代理', 0, NULL, '2020-10-29 09:33:47', 2);
INSERT INTO `xeg_role` VALUES (3, '虚拟市场老板', 0, NULL, '2020-10-29 09:34:00', 3);

-- ----------------------------
-- Table structure for xeg_role_rule
-- ----------------------------
DROP TABLE IF EXISTS `xeg_role_rule`;
CREATE TABLE `xeg_role_rule`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT,
  `role_id` int(0) NOT NULL COMMENT '角色Id',
  `rule_id` int(0) NOT NULL COMMENT '权限Id',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '0未删除 1已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色权限关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_role_rule
-- ----------------------------
INSERT INTO `xeg_role_rule` VALUES (1, 2, 1, 0, '2020-11-19 16:05:09');

-- ----------------------------
-- Table structure for xeg_rule
-- ----------------------------
DROP TABLE IF EXISTS `xeg_rule`;
CREATE TABLE `xeg_rule`  (
  `rule_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `rule_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `rule_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限图标',
  `parent_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父名称',
  `pid` int(0) NULL DEFAULT 0 COMMENT '父级ID',
  `rule_pathinfo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限路径',
  `rule_type` int(0) NULL DEFAULT NULL COMMENT '权限类型( 1 大菜单2子菜单 3 按钮)',
  `rule_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '权限状态( 0关闭 1开启)',
  `rule_sort` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `rule_create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `rule_update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `rule_remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `deleted` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除(0 删除,1未删)',
  `delete_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '删除时间',
  PRIMARY KEY (`rule_id`) USING BTREE,
  INDEX `group_id`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 161 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_rule
-- ----------------------------
INSERT INTO `xeg_rule` VALUES (1, '首页', 'home', '顶级菜单', 0, '/admin/home', 1, 1, 1, '2020-09-16 23:56:35', NULL, '备注1', 0, '2020-09-28 08:52:30');
INSERT INTO `xeg_rule` VALUES (2, '系统管理', 'user', '顶级菜单', 0, '/admin/system', 1, 1, 2, '2020-09-17 08:29:13', NULL, '备注2', 0, '2020-09-28 08:52:33');
INSERT INTO `xeg_rule` VALUES (3, '用户管理', 'usermanage', '顶级菜单', 0, '/admin/user', 1, 1, 3, '2020-09-17 08:29:14', NULL, '备注3', 0, '2020-09-28 08:52:36');
INSERT INTO `xeg_rule` VALUES (4, '订单管理', 'ordermanage', '顶级菜单', 0, '/admin/order', 1, 1, 4, '2020-09-18 08:20:59', NULL, '备注4', 0, '2020-09-28 08:29:23');
INSERT INTO `xeg_rule` VALUES (5, '平台参数', 'parameter', '顶级菜单', 0, '/admin/parameter', 1, 1, 5, '2020-09-18 08:21:01', NULL, '备注5', 0, '2020-09-28 08:29:26');
INSERT INTO `xeg_rule` VALUES (6, '账号管理', NULL, '系统管理', 2, '/admin/system/user-list', 2, 1, 1, '2020-09-18 08:21:03', '2020-10-17 09:24:18', '备注6', 0, '2020-09-28 08:29:32');
INSERT INTO `xeg_rule` VALUES (7, '角色管理', NULL, '系统管理', 2, '/admin/system/role', 2, 1, 2, '2020-09-18 08:21:05', NULL, '备注7', 0, '2020-09-28 08:29:30');
INSERT INTO `xeg_rule` VALUES (8, '菜单管理', NULL, '系统管理', 2, '/admin/system/menu', 2, 1, 3, '2020-09-18 08:21:07', NULL, '备注8', 0, '2020-09-28 08:29:36');
INSERT INTO `xeg_rule` VALUES (9, '系统参数', NULL, '系统管理', 2, '/admin/system/config', 2, 1, 4, '2020-09-18 08:21:08', NULL, '备注9', 0, '2020-09-28 08:29:39');
INSERT INTO `xeg_rule` VALUES (14, '技能设置', NULL, '平台参数', 5, '/admin/parameter/skill', 2, 1, 2, '2020-09-18 08:21:38', NULL, '备注14', 0, '2020-09-28 08:29:53');
INSERT INTO `xeg_rule` VALUES (15, '代理管理', 'user', '顶级菜单', 0, '/admin/agent', 1, 1, 6, '2020-10-17 09:26:54', NULL, '备注15', 0, '2020-10-17 09:26:54');
INSERT INTO `xeg_rule` VALUES (16, '代理列表', '', '代理管理', 15, '/admin/agent/agent-list', 2, 1, 1, '2020-10-17 09:28:19', '2020-10-17 09:29:56', '备注16', 0, '2020-10-17 09:28:19');
INSERT INTO `xeg_rule` VALUES (17, '雇主列表', '', '用户管理', 3, '/admin/user/boss-manage', 2, 1, 1, '2020-10-17 09:29:38', '2020-10-17 09:30:00', '备注17', 0, '2020-10-17 09:29:38');
INSERT INTO `xeg_rule` VALUES (18, '雇工列表', '', '用户管理', 3, '/admin/user/worker-manage', 2, 1, 2, '2020-10-17 09:30:24', '2020-10-17 09:35:33', '备注18', 0, '2020-10-17 09:30:24');
INSERT INTO `xeg_rule` VALUES (19, '订单列表', '', '订单管理', 4, '/admin/order/order-manage', 2, 1, 1, '2020-10-17 09:34:56', '2020-10-17 09:35:27', '备注19', 0, '2020-10-17 09:34:56');
INSERT INTO `xeg_rule` VALUES (20, '总评价列表', '', '订单管理', 4, '/admin/order/evaluate', 2, 1, 3, '2020-10-17 09:36:06', NULL, '备注20', 0, '2020-10-17 09:36:06');
INSERT INTO `xeg_rule` VALUES (24, '设置参数', NULL, '平台参数', 5, '/admin/parameter/config', 2, 1, 1, '2020-10-17 09:42:13', NULL, '备注24', 0, '2020-10-17 09:42:13');
INSERT INTO `xeg_rule` VALUES (26, '资讯管理', 'news', '顶级菜单', 0, '/admin/new', 1, 1, 8, '2020-10-17 09:44:46', NULL, '备注26', 0, '2020-10-17 09:44:46');
INSERT INTO `xeg_rule` VALUES (27, '资讯管理', '', '资讯管理', 26, '/admin/new/news', 2, 1, 1, '2020-10-17 09:44:47', '2020-10-17 09:45:57', '备注27', 0, '2020-10-17 09:44:47');
INSERT INTO `xeg_rule` VALUES (28, 'APP管理', 'app', '顶级菜单', 0, '/admin/app', 1, 1, 9, '2020-10-17 09:47:12', NULL, '备注28', 0, '2020-10-17 09:47:12');
INSERT INTO `xeg_rule` VALUES (29, '意见反馈', '', 'APP管理', 28, '/admin/app/feedback', 2, 1, 1, '2020-10-17 09:47:57', '2020-10-17 09:47:57', '备注29', 0, '2020-10-17 09:47:57');
INSERT INTO `xeg_rule` VALUES (30, '投诉管理', '', 'APP管理', 28, '/admin/app/complaint', 2, 1, 2, '2020-10-17 09:48:38', '2020-10-17 09:48:38', '备注30', 0, '2020-10-17 09:48:38');
INSERT INTO `xeg_rule` VALUES (31, '投诉选项', '', 'APP管理', 28, '/admin/app/complaint-options', 2, 1, 3, '2020-10-17 09:49:20', '2020-10-17 09:49:20', '备注31', 0, '2020-10-17 09:49:20');
INSERT INTO `xeg_rule` VALUES (32, '首页-我的菜单', '', 'APP管理', 28, '/admin/app/home-menus', 2, 1, 4, '2020-10-17 09:49:54', '2020-10-17 09:50:12', '备注32', 0, '2020-10-17 09:49:54');
INSERT INTO `xeg_rule` VALUES (33, '我的-菜单管理', '', 'APP管理', 28, '/admin/app/my-menus', 2, 1, 5, '2020-10-17 09:50:13', '2020-10-17 09:51:02', '备注33', 0, '2020-10-17 09:50:13');
INSERT INTO `xeg_rule` VALUES (34, '招工方式管理', '', 'APP管理', 28, '/admin/app/recruit', 2, 1, 6, '2020-10-17 09:51:44', '2020-10-17 09:51:44', '备注34', 0, '2020-10-17 09:51:44');
INSERT INTO `xeg_rule` VALUES (35, '版本更新', '', 'APP管理', 28, '/admin/app/versions', 2, 1, 7, '2020-10-17 09:52:23', NULL, '备注35', 0, '2020-10-17 09:52:23');
INSERT INTO `xeg_rule` VALUES (36, '用户协议', '', 'APP管理', 28, '/admin/app/user-agreement', 2, 1, 8, '2020-10-17 09:52:56', '2020-10-17 14:06:18', '备注36', 0, '2020-10-17 09:52:56');
INSERT INTO `xeg_rule` VALUES (37, '财务管理', 'news', '顶级菜单', 0, '/admin/plat', 1, 1, 10, '2020-10-17 09:53:52', NULL, '备注37', 0, '2020-10-17 09:53:52');
INSERT INTO `xeg_rule` VALUES (38, '账单列表', '', '财务管理', 37, '/admin/finance/finance-list', 2, 1, 1, '2020-10-17 09:54:52', '2020-10-17 09:55:26', '备注38', 0, '2020-10-17 09:54:52');
INSERT INTO `xeg_rule` VALUES (39, '提现审核', NULL, '财务管理', 37, '/admin/app/withdrawal', 2, 1, 2, '2020-10-17 09:56:07', '2020-10-17 10:55:48', '备注39', 0, '2020-10-17 09:56:07');
INSERT INTO `xeg_rule` VALUES (40, '密码管理', '', '系统管理', 2, '/admin/system/password', 2, 1, 5, '2020-10-20 17:15:13', '2020-11-10 19:02:40', '修改密码', 0, '2020-10-20 17:15:13');
INSERT INTO `xeg_rule` VALUES (148, '主题管理', '', '资讯管理', 26, '/admin/new/theme', 2, 1, 2, '2020-10-29 13:52:51', '2020-10-29 13:52:51', '资讯里用到的主题', 0, '2020-10-29 13:52:51');
INSERT INTO `xeg_rule` VALUES (149, '消息模板', '', '平台消息', 25, '/admin/platform/template', 2, 1, 1, '2020-10-31 09:18:32', '2020-10-31 09:18:32', '平台用的消息之类的', 0, '2020-10-31 09:18:32');
INSERT INTO `xeg_rule` VALUES (150, '话术消息', '', '平台消息', 25, '/admin/platform/skill', 2, 1, 2, '2020-10-31 09:22:43', '2020-10-31 09:22:43', '话术', 0, '2020-10-31 09:22:43');
INSERT INTO `xeg_rule` VALUES (151, '消息模板', '', '平台参数', 5, '/admin/platform/template', 2, 1, 3, '2020-10-31 16:32:44', '2020-10-31 16:32:44', '消息模板', 0, '2020-10-31 16:32:44');
INSERT INTO `xeg_rule` VALUES (152, '话术模板', '', '平台参数', 5, '/admin/platform/skill', 2, 1, 4, '2020-10-31 16:33:10', '2020-10-31 16:33:10', '话术', 0, '2020-10-31 16:33:10');
INSERT INTO `xeg_rule` VALUES (153, '地区管理', '', '系统管理', 2, '/admin/system/area', 2, 1, 6, '2020-11-01 14:22:10', '2020-11-01 14:22:10', '可添加四级地区', 0, '2020-11-01 14:22:10');
INSERT INTO `xeg_rule` VALUES (154, 'APP菜单配置', '', '顶级菜单', 0, '/admin/app/app-menus-config', 1, 1, 9, '2020-11-01 19:26:24', '2020-11-01 19:27:17', 'APP雇工菜单配置', 1, '2020-11-01 19:26:24');
INSERT INTO `xeg_rule` VALUES (155, 'APP菜单推荐', '', 'APP管理', 28, '/admin/app/app-menus-config', 2, 1, 9, '2020-11-01 19:27:11', '2020-11-07 16:03:05', 'APP雇工菜单是否推荐', 1, '2020-11-01 19:27:11');
INSERT INTO `xeg_rule` VALUES (156, 'OSS素材', '', '系统管理', 2, '/admin/system/oss', 2, 1, 7, '2020-11-03 13:41:06', '2020-11-03 13:41:06', '维护oss上的图片', 0, '2020-11-03 13:41:06');
INSERT INTO `xeg_rule` VALUES (157, '直播管理', 'el-icon-mic', '顶级菜单', 0, '/admin/live', 1, 1, 11, '2020-11-03 17:09:37', NULL, '直播', 0, '2020-11-03 17:09:37');
INSERT INTO `xeg_rule` VALUES (158, '直播间列表', '', '直播管理', 157, '/admin/live/live-list', 2, 1, 1, '2020-11-03 17:10:09', '2020-11-03 17:10:09', '直播间列表', 0, '2020-11-03 17:10:09');
INSERT INTO `xeg_rule` VALUES (159, '工作内容模板', '', '订单管理', 4, '/admin/order/content-template', 2, 1, 2, '2020-11-04 09:22:42', '2020-11-04 09:22:42', '订单工作内容模板', 0, '2020-11-04 09:22:42');
INSERT INTO `xeg_rule` VALUES (160, '主持人列表', '', '直播管理', 157, '/admin/live/host', 2, 1, 2, '2020-11-05 15:19:54', '2020-11-05 15:19:54', NULL, 0, '2020-11-05 15:19:54');

-- ----------------------------
-- Table structure for xeg_user_agent
-- ----------------------------
DROP TABLE IF EXISTS `xeg_user_agent`;
CREATE TABLE `xeg_user_agent`  (
  ` bid` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NOT NULL COMMENT '用户Id',
  `user_identity_id` int(0) NOT NULL COMMENT '用户身份关联表Id',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '0未删除 1已删除',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (` bid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户上级代理关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_user_agent
-- ----------------------------

-- ----------------------------
-- Table structure for xeg_user_identity
-- ----------------------------
DROP TABLE IF EXISTS `xeg_user_identity`;
CREATE TABLE `xeg_user_identity`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NOT NULL DEFAULT 0 COMMENT 'userId/agentId',
  `identity_id` int(0) NOT NULL DEFAULT 0 COMMENT '身份编号',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '手机号',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0 未删除  1已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`bid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户身份关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_user_identity
-- ----------------------------

-- ----------------------------
-- Table structure for xeg_user_info
-- ----------------------------
DROP TABLE IF EXISTS `xeg_user_info`;
CREATE TABLE `xeg_user_info`  (
  `bid` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码\nMD5 加密',
  `sex` tinyint(0) NOT NULL DEFAULT 0 COMMENT '性别 0 其它 1男 2女',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '注册时间',
  `birth_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '出生年月1990-06-02',
  `age` tinyint(0) NOT NULL DEFAULT 0 COMMENT '年龄',
  `state` tinyint(0) NOT NULL DEFAULT 0 COMMENT '状态 0正常 1禁用',
  `is_black` tinyint(0) NOT NULL DEFAULT 0 COMMENT '黑名单 0否 1是',
  `is_del` int(0) NOT NULL DEFAULT 1 COMMENT '1显示0不显示',
  `registered_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '注册身份(1雇工 2雇）',
  `invite_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邀请码',
  `pay_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '支付密码',
  `card_front` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '身份证正面',
  `card_back` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '身份证反面',
  `agent_id` int(0) NOT NULL DEFAULT 0 COMMENT '代理Id',
  `identity_status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '身份状态 0 普通用户  1 特殊身份',
  PRIMARY KEY (`bid`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xeg_user_info
-- ----------------------------
INSERT INTO `xeg_user_info` VALUES (1, '15000000000', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 18:23:54', '1993-09-09', 30, 0, 0, 1, 0, '', '', '', '', 5, 1);
INSERT INTO `xeg_user_info` VALUES (2, '18000000000', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 18:24:46', '', 30, 0, 0, 1, 0, '', '', '', '', 6, 1);
INSERT INTO `xeg_user_info` VALUES (3, '15868804722', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:42:52', '1990-11-10', 0, 0, 0, 1, 2, '7kb7qm', '', '', '', 7, 1);
INSERT INTO `xeg_user_info` VALUES (4, '13156376615', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:49:32', '1997-12-08', 0, 0, 0, 1, 2, 'pzn6bs', '', '', '', 8, 1);
INSERT INTO `xeg_user_info` VALUES (5, '18678967543', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:52:18', '2002-11-10', 0, 0, 0, 1, 2, 'j2eu5y', '', '', '', 9, 1);
INSERT INTO `xeg_user_info` VALUES (6, '18866216142', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:52:36', '1994-10-10', 0, 0, 0, 1, 2, '8t6lja', '', '', '', 0, 0);
INSERT INTO `xeg_user_info` VALUES (7, '15763335457', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:52:40', '1995-01-05', 0, 0, 0, 1, 2, '5ga7xz', '', '', '', 0, 0);
INSERT INTO `xeg_user_info` VALUES (8, '15563988405', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:52:42', '1998-11-10', 0, 0, 0, 1, 2, '6iqt9t', '', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605013623104881918.png', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605013608813324457.png', 0, 0);
INSERT INTO `xeg_user_info` VALUES (9, '13053636481', 'e10adc3949ba59abbe56e057f20f883e', 2, '2020-11-10 19:52:55', '1995-11-10', 0, 0, 0, 1, 2, 'cacd26', '', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605010335391313735.jpeg', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605010340596686908.jpeg', 0, 0);
INSERT INTO `xeg_user_info` VALUES (10, '18366335922', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:53:00', '1987-11-10', 0, 0, 0, 1, 2, 'upmhxp', '', '', '', 0, 0);
INSERT INTO `xeg_user_info` VALUES (11, '17860905251', 'e10adc3949ba59abbe56e057f20f883e', 2, '2020-11-10 19:53:01', '1999-04-21', 0, 0, 0, 1, 2, 'vbemn2', '', '', '', 0, 0);
INSERT INTO `xeg_user_info` VALUES (12, '18769817791', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:53:05', '1999-05-19', 0, 0, 0, 1, 2, 'xfvng8', '', '', '', 0, 0);
INSERT INTO `xeg_user_info` VALUES (13, '13406730807', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:53:43', '1998-01-01', 0, 0, 0, 1, 1, 'ngr9m4', '', '', '', 10, 1);
INSERT INTO `xeg_user_info` VALUES (14, '18678972614', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:56:53', '1995-11-17', 0, 0, 0, 1, 2, '5yze7j', '', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605013078557593239.jpeg', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605013090841645869.jpeg', 0, 0);
INSERT INTO `xeg_user_info` VALUES (15, '18053754825', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:58:54', '1999-11-10', 0, 0, 0, 1, 1, 'b9fzkc', '', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605009830662390890.png', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605009836362083775.png', 0, 0);
INSERT INTO `xeg_user_info` VALUES (16, '15689987293', 'e807f1fcf82d132f9bb018ca6738a19f', 1, '2020-11-10 19:59:17', '1984-11-10', 0, 0, 0, 1, 1, 'bveb78', '', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605010506160175924.png', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605010521762198915.png', 0, 0);
INSERT INTO `xeg_user_info` VALUES (17, '18562737532', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 19:59:47', '1902-01-01', 0, 0, 0, 1, 2, 'p495c1', '', '', '', 0, 0);
INSERT INTO `xeg_user_info` VALUES (18, '15053657211', 'e10adc3949ba59abbe56e057f20f883e', 2, '2020-11-10 20:04:22', '1996-11-10', 0, 0, 0, 1, 2, 'yu5a44', '', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605010426617660669.png', 'http://xeg.oss-cn-beijing.aliyuncs.com/card/1605010435440474705.png', 0, 0);
INSERT INTO `xeg_user_info` VALUES (19, '18810069046', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-11-10 21:16:53', '1996-09-10', 0, 0, 0, 1, 2, 'ar4agb', '', '', '', 0, 0);

SET FOREIGN_KEY_CHECKS = 1;
