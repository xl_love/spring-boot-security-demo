package com.sans;

import com.sans.core.entity.Admin;
import com.sans.core.entity.AdminRole;
import com.sans.core.entity.Identity;
import com.sans.core.service.AdminRoleService;
import com.sans.core.service.AdminService;
import com.sans.core.service.IdentityService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootSecurityDemoApplicationTests {


    @Autowired
    private AdminService adminService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private AdminRoleService adminRoleService;
    @Autowired
    private IdentityService identityService;

    /**
     * 注册用户
     */
    @Test
    public void contextLoads() {
        // 注册用户
        Admin admin = new Admin();
        admin.setAccount("sans");
        admin.setPassword(bCryptPasswordEncoder.encode("123456"));
        admin.setUsername("sans");
        // 设置用户状态
        admin.setAdminStatus(1);
        admin.setBid(9);
        adminService.save(admin);
        // 分配角色 1:ADMIN 2:USER
        AdminRole adminRole = new AdminRole();
        adminRole.setBid(1);
        adminRole.setRoleId(2);
        adminRole.setAdminId(admin.getBid());
        adminRoleService.save(adminRole);
    }

    @Test
    public void testIdentity(){
        //查询用户身份信息
        String ident = "1";
        List<Identity> identityList = identityService.selectSysIdentityByUserId(9);//[Identity(bid=1, identityType=1, identityName=劳务市场, status=2, isDel=0, createTime=2020-11-20 16:43:54.0)]
        for (Identity identity : identityList) {
            System.err.println(identity.getIdentityType());
            System.err.println(ident);
            boolean contains = StringUtils.contains(ident, identity.getIdentityType());
            System.err.println(contains);//false
            if (contains){
                throw new InsufficientAuthenticationException("用户身份信息不存在");
            }
        }
        /*boolean contains = StringUtils.contains(identityList.toString(),ident);
        if (!contains){
            throw new InsufficientAuthenticationException("用户身份信息不存在");
        }*/
        System.err.println("用户身份存在!");
    }
    @Test
    public void test(){
        List<Identity> list = new ArrayList();
        Identity identity1 = new Identity(1,1,"劳务市场",1,0,new Timestamp(System.currentTimeMillis()));
        Identity identity2 = new Identity(2,2,"劳务派遣",1,0,new Timestamp(System.currentTimeMillis()));
        list.add(identity1);
        list.add(identity2);
        System.err.println(list.toString());
        Integer str = 1;
        for (Identity identity : list) {
            Integer identityType = identity.getIdentityType();
            if (identityType==str){
                System.err.println("OK");
            }
        }
        boolean contains = StringUtils.contains(list.toString(), str);
        System.err.println(contains);
    }
}