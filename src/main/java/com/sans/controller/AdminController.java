package com.sans.controller;

import com.sans.common.util.ResultUtil;
import com.sans.common.util.SecurityUtil;
import com.sans.core.entity.Rule;
import com.sans.core.entity.Role;
import com.sans.core.entity.Admin;
import com.sans.core.service.RuleService;
import com.sans.core.service.RoleService;
import com.sans.core.service.AdminService;
import com.sans.security.entity.SelfUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 管理端
 * @author 123
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RuleService ruleService;

    /**
     * 管理端信息
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/info",method = RequestMethod.GET)
    public Map<String,Object> userLogin(){
        Map<String,Object> result = new HashMap<>();
        SelfUserEntity userDetails = SecurityUtil.getUserInfo();
        result.put("title","管理端信息");
        result.put("data",userDetails);
        return ResultUtil.resultSuccess(result);
    }

    /**
     * 拥有ADMIN或者USER角色可以访问
     */
    @PreAuthorize("hasAnyRole('ADMIN','代理')")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public Map<String,Object> list(){
        Map<String,Object> result = new HashMap<>();
        List<Admin> adminList = adminService.list();
        result.put("title","拥有用户或者管理员角色都可以查看");
        result.put("data", adminList);
        return ResultUtil.resultSuccess(result);
    }

    /**
     * 拥有ADMIN和USER角色可以访问
     */
    @PreAuthorize("hasRole('ADMIN') and hasRole('USER')")
    @RequestMapping(value = "/menuList",method = RequestMethod.GET)
    public Map<String,Object> menuList(){
        Map<String,Object> result = new HashMap<>();
        List<Rule> ruleList = ruleService.list();
        result.put("title","拥有用户和管理员角色都可以查看");
        result.put("data", ruleList);
        return ResultUtil.resultSuccess(result);
    }


    /**
     * 拥有sys:user:info权限可以访问
     * hasPermission 第一个参数是请求路径 第二个参数是权限表达式
     */
    @PreAuthorize("hasPermission('/admin/userList','/admin/home')")
    @RequestMapping(value = "/userList",method = RequestMethod.GET)
    public Map<String,Object> userList(){
        Map<String,Object> result = new HashMap<>();
        List<Admin> adminList = adminService.list();
        result.put("title","拥有sys:user:info权限都可以查看");
        result.put("data", adminList);
        return ResultUtil.resultSuccess(result);
    }


    /**
     * 拥有ADMIN角色和sys:role:info权限可以访问
     */
    @PreAuthorize("hasRole('ADMIN') and hasPermission('/admin/adminRoleList','sys:role:info')")
    @RequestMapping(value = "/adminRoleList",method = RequestMethod.GET)
    public Map<String,Object> adminRoleList(){
        Map<String,Object> result = new HashMap<>();
        List<Role> roleList = roleService.list();
        result.put("title","拥有ADMIN角色和sys:role:info权限可以访问");
        result.put("data", roleList);
        return ResultUtil.resultSuccess(result);
    }
}