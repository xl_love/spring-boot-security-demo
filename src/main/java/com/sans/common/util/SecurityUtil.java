package com.sans.common.util;

import com.sans.core.entity.Identity;
import com.sans.security.entity.SelfUserEntity;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

/**
 * Security工具类
 * @author 123
 */
public class SecurityUtil {

    /**
     * 私有化构造器
     */
    private SecurityUtil(){}

    /**
     * 获取当前用户信息
     */
    public static SelfUserEntity getUserInfo(){
        SelfUserEntity userDetails = (SelfUserEntity) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        return userDetails;
    }
    /**
     * 获取当前用户ID
     */
    public static Integer getBid(){
        return getUserInfo().getBid();
    }
    /**
     * 获取当前用户账号
     */
    public static String getAccount(){
        return getUserInfo().getUsername();
    }
    /**
     * 获取当前身份信息
     */
    public static Collection<Identity> getIdentityType(){
        return getUserInfo().getIdentities();
    }
}