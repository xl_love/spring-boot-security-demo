package com.sans.security.service;

import com.sans.core.entity.Admin;
import com.sans.core.service.AdminService;
import com.sans.security.entity.SelfUserEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * SpringSecurity用户的业务实现
 * @author 123
 */
@Component
public class SelfUserDetailsService implements UserDetailsService {

    @Autowired
    private AdminService adminService;

    /**
     * 查询用户信息
     */
    @Override
    public SelfUserEntity loadUserByUsername(String account) throws UsernameNotFoundException {
        // 查询用户信息
        Admin admin = adminService.selectUserByName(account);
        if (admin !=null){
            // 组装参数
            SelfUserEntity selfUserEntity = new SelfUserEntity();
            BeanUtils.copyProperties(admin,selfUserEntity);
            return selfUserEntity;
        }
        return null;
    }
}