package com.sans.core.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author 123
 */
@Data
@TableName("xeg_agent_identity")
public class AgentIdentity {
    private Integer bid;
    private Integer agentId;
    private Integer identityId;
    private String phone;
    private Integer isDel;
    private Timestamp createTime;
    private Integer roleId;
    private Integer charge;
}
