package com.sans.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.util.Assert;

import java.sql.Timestamp;

/**
 * @author 123
 */
@Data
@TableName("xeg_identity")
public class Identity {
    @TableId
    private Integer bid;
    private Integer identityType;
    private String identityName;
    private Integer status;
    private Integer isDel;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp createTime;
    private Integer agentId;
    private Integer identityId;

    public Identity(String identityName) {
        Assert.hasText(identityName, "A textual representation of an identity that requires authentication");
        this.identityName = identityName;
    }

    public Identity() {
    }

    public Identity(Integer bid, Integer identityType, String identityName, Integer status, Integer isDel, Timestamp createTime) {
        this.bid = bid;
        this.identityType = identityType;
        this.identityName = identityName;
        this.status = status;
        this.isDel = isDel;
        this.createTime = createTime;
    }
}
