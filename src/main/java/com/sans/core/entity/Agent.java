package com.sans.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.sql.Timestamp;

/**
 * 代理信息
 * @author 123
 */
@Data
@TableName("xeg_agent")
public class Agent {
    @TableId
    private Integer bid;
    private Integer adminId;
    private String agentName;
    private String createUser;
    private Integer parentId;
    private Integer agentType;
    private String areaCode;
    private String inviteCode;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp createTime;
    private Integer isDel;
}
