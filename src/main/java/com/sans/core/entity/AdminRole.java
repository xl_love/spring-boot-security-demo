package com.sans.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *  账号与角色关系实体
 * @author 123
 */
@Data
@TableName("xeg_admin_role")
public class AdminRole implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@TableId
	private Integer bid;
	/**
	 * 用户ID
	 */
	private Integer adminId;
	/**
	 * 角色ID
	 */
	private Integer roleId;
	/**
	 * 状态
	 */
	private Integer isDel;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Timestamp createTime;
}
