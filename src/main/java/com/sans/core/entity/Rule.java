package com.sans.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 权限实体
 * @author 123
 */
@Data
@TableName("xeg_rule")
public class Rule implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 权限ID
	 */
	@TableId
	private Long ruleId;
	/**
	 * 权限名称
	 */
	private String ruleName;
	/**
	 * 权限图标
	 */
	private String ruleIcon;
	/**
	 * 父名称
	 */
	private String parentName;
	private Integer pid;
	/**
	 * 权限标识
	 */
	//private String permission;
	private String rulePathinfo;
	private Integer ruleType;
	private Integer ruleStatus;
	private Integer ruleSort;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Timestamp ruleCreateTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Timestamp ruleUpdateTime;
	private String ruleRemarks;
	private Integer deleted;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Timestamp deleteTime;
}
