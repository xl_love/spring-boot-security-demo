package com.sans.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sans.core.entity.Rule;
import com.sans.core.entity.Role;
import com.sans.core.entity.Admin;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * @Description 系统用户DAO
 * @Author Sans
 * @CreateTime 2019/9/14 15:57
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

    /**
     * 通过用户ID查询角色集合
     */
    List<Role> selectSysRoleByUserId(Integer agentId,Integer identityId);
    /**
     * 通过用户ID查询权限集合
     */
    List<Rule> selectSysMenuByUserId(Integer bid);

    /**
     * 根据输入的用户名查询用户信息
     * @param account account
     * @return admin
     */
    Admin selectUserByName(String account);

}
