package com.sans.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sans.core.entity.AdminRole;
import com.sans.core.entity.Identity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 身份
 * @author 123
 */
@Mapper
public interface IdentityMapper extends BaseMapper<Identity> {
    List<Identity> selectSysIdentityByUserId(Integer bid);
}
