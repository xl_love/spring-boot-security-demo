package com.sans.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sans.core.entity.AdminRole;
import com.sans.core.entity.AgentIdentity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 代理身份信息认证
 * @author 123
 */
@Mapper
public interface AgentIdentityMapper extends BaseMapper<AgentIdentity> {
}
