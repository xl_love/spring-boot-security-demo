package com.sans.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sans.core.entity.AdminRole;
import com.sans.core.entity.Agent;
import org.apache.ibatis.annotations.Mapper;

/**
 * 代理信息
 * @author 123
 */
@Mapper
public interface AgentMapper extends BaseMapper<Agent> {
}
