package com.sans.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.AdminMapper;
import com.sans.core.entity.Rule;
import com.sans.core.entity.Role;
import com.sans.core.entity.Admin;
import com.sans.core.service.AdminService;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * 系统用户业务实现
 * @author 123
 */
@Service("AdminService")
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    /**
     * 根据用户名查询实体
     */
    @Override
    public Admin selectUserByName(String account) {
        return this.baseMapper.selectUserByName(account);
    }
    /**
     * 通过用户ID查询角色集合
     */
    @Override
    public List<Role> selectSysRoleByUserId(Integer agentId,Integer identityId) {
        return this.baseMapper.selectSysRoleByUserId(agentId,identityId);
    }

    /**
     * 根据用户ID查询权限集合
     */
    @Override
    public List<Rule> selectSysMenuByUserId(Integer bid) {
        return this.baseMapper.selectSysMenuByUserId(bid);
    }
}