package com.sans.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.IdentityMapper;
import com.sans.core.entity.Identity;
import com.sans.core.service.IdentityService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 身份
 * @author 123
 */
@Service("IdentityService")
public class IdentityServiceImpl extends ServiceImpl<IdentityMapper, Identity> implements IdentityService {

    /**
     * 根据
     * @param bid bid
     * @return list
     */
    @Override
    public List<Identity> selectSysIdentityByUserId(Integer bid) {
        return this.baseMapper.selectSysIdentityByUserId(bid);
    }
}
