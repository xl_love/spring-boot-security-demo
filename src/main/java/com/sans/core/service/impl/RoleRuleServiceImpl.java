package com.sans.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.RoleRuleMapper;
import com.sans.core.entity.RoleRule;
import com.sans.core.service.RoleRuleService;
import org.springframework.stereotype.Service;

/**
 * 角色与权限业务实现
 * @author 123
 */
@Service("RoleRuleService")
public class RoleRuleServiceImpl extends ServiceImpl<RoleRuleMapper, RoleRule> implements RoleRuleService {

}