package com.sans.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.RuleMapper;
import com.sans.core.entity.Rule;
import com.sans.core.service.RuleService;
import org.springframework.stereotype.Service;

/**
 *  权限业务实现
 * @author 123
 */
@Service("RuleService")
public class RuleServiceImpl extends ServiceImpl<RuleMapper, Rule> implements RuleService {

}