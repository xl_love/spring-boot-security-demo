package com.sans.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.AdminRoleMapper;
import com.sans.core.entity.AdminRole;
import com.sans.core.service.AdminRoleService;
import org.springframework.stereotype.Service;

/**
 * 用户与角色业务实现
 * @author 123
 */
@Service("AdminRoleService")
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole> implements AdminRoleService {

}