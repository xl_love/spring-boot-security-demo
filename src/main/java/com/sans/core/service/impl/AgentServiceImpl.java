package com.sans.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.AgentMapper;
import com.sans.core.entity.Agent;
import com.sans.core.service.AgentService;
import org.springframework.stereotype.Service;

/**
 * 代理
 * @author 123
 */
@Service("AgentService")
public class AgentServiceImpl extends ServiceImpl<AgentMapper, Agent> implements AgentService {
}
