package com.sans.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.RoleMapper;
import com.sans.core.entity.Role;
import com.sans.core.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * 角色业务实现
 * @author 123
 */
@Service("RoleService")
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}