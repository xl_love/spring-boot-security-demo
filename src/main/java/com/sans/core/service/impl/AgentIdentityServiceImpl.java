package com.sans.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sans.core.dao.AgentIdentityMapper;
import com.sans.core.entity.AgentIdentity;
import com.sans.core.service.AgentIdentityService;
import org.springframework.stereotype.Service;

/**
 * 代理身份信息
 * @author 123
 */
@Service("AgentIdentityService")
public class AgentIdentityServiceImpl extends ServiceImpl<AgentIdentityMapper, AgentIdentity> implements AgentIdentityService {

}
