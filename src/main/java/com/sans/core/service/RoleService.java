package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.Role;

/**
 *  角色业务接口
 * @author 123
 */
public interface RoleService extends IService<Role> {

}