package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.Rule;
import com.sans.core.entity.Role;
import com.sans.core.entity.Admin;
import java.util.List;

/**
 *  系统用户业务接口
 * @author 123
 */
public interface AdminService extends IService<Admin>  {

    /**
     * 根据用户名查询实体
     * @param account account
     * @return admin
     */
    Admin selectUserByName(String account);
    /**
     * 根据用户ID查询角色集合
     * @param identityId bid
     * @return list
     */
    List<Role> selectSysRoleByUserId(Integer agentId,Integer identityId);
    /**
     * 根据用户ID查询权限集合
     * @param bid bid
     * @return list
     */
    List<Rule> selectSysMenuByUserId(Integer bid);

}