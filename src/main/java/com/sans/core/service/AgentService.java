package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.Agent;

/**
 * 代理
 * @author 123
 */
public interface AgentService extends IService<Agent> {
}
