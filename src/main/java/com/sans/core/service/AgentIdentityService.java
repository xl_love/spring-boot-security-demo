package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.AgentIdentity;

/**
 * 代理身份
 * @author 123
 */
public interface AgentIdentityService extends IService<AgentIdentity> {
}
