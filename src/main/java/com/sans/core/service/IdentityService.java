package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.Identity;

import java.util.List;

/**
 * 身份
 * @author 123
 */
public interface IdentityService extends IService<Identity> {
    /**
     * 根据用户ID查询身份集合
     * @param bid bid
     * @return list
     */
    List<Identity> selectSysIdentityByUserId(Integer bid);
}
