package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.RoleRule;

/**
 *  角色与权限业务接口
 * @author 123
 */
public interface RoleRuleService extends IService<RoleRule> {

}