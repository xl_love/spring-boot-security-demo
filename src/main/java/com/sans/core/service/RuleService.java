package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.Rule;

/**
 * 权限业务接口
 * @author 123
 */
public interface RuleService extends IService<Rule> {

}