package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.AdminRole;

/**
 * 用户与角色业务接口
 * @author 123
 */
public interface AdminRoleService extends IService<AdminRole> {

}